#ifndef LIST_H
#define LIST_H
#include "Sequence.h"
#include <vector>
#include <memory>

class List : public Sequence
{
public:

	List() = default;

	void append(std::shared_ptr<Object> object);
	virtual std::string toString() const override;
	virtual Types getType() const override;
	virtual std::string getTypeString() const override { return "List"; };
	virtual unsigned int getLength() const override { return m_Objects.size(); }
private:
	std::vector<std::shared_ptr<Object>> m_Objects;
};
#endif // LIST_H