#pragma once
#include "Object.h"


class Sequence :
	public Object
{
public:
	virtual Types getType() const override;
	virtual unsigned int getLength() const = 0;

protected:
};

