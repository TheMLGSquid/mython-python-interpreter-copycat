#include "parser.h"
#include <regex>
#include <iostream>
#include "IndentationException.h"
#include <string_view>
#include <algorithm>
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "List.h"
#include "Void.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "TypeExcpetion.h"


static std::regex s_ListExpression(
	R"regex(^\[()|((([0-9A-Za-z_\-]*)|([\"\'].*[\"\']))(, (([0-9A-Za-z_\-]+)|([\"\'].*[\"\'])))*)\]$)regex");
static std::regex s_ListElementExpression(R"regex((([0-9A-Za-z_\-]*)|([\"\'].*[\"\'])))regex");

static std::regex s_TypeFunctionExpression("type(.*)");
static std::regex s_SizeFunctionExpression("len(.*)");

static unsigned int CharacterAmount(const std::string& string, char character)
{
	return std::ranges::count(string, character);
}
static void Ltrim(std::string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}


static void Rtrim(std::string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}
static void Trim(std::string &str)
{
	Rtrim(str);
	Ltrim(str);

}

static std::string Trim(const std::string& str)
{
	std::string newString = str;
	Trim(newString);
	return newString;
}




static bool IsNumber(const std::string& string)
{	
	int startIndexCheck = 0;
	if (string.empty())
	{
		return false;
	}
	
	if(string[0] == '-' && CharacterAmount(string, '-') == 1)
	{
		startIndexCheck = 1;
	}
	
	return std::all_of(string.begin() + startIndexCheck, string.end(), [](char character)
	{
			return character >= '0' && character <= '9';
	});
}

static bool IsBoolean(const std::string& string)
{
	if(string.empty())
	{
		return false;
	}

	return (string == "True" || string == "False");
}


static bool IsString(const std::string& string)
{
	if(string.empty())
	{
		return false;
	}

	if(string.front() == '"' && string.back() == '"')
	{
		return CharacterAmount(string, '"') == 2;
	}

	if(string.front() == '\'' && string.back() == '\'')
	{
		return CharacterAmount(string, '\'') == 2;
	}

	return false;
}


static bool IsList(const std::string& string)
{
	
	return std::regex_search(string, s_ListExpression);
}

std::shared_ptr<Object> Parser::parseString(std::string str, int depth)
{
	bool continueCheckingValue = true;
	std::shared_ptr<Object> returnValue = nullptr;
	if(str.empty())
	{
		return nullptr;
	}

	if (str[0] == '\t' || str[0] == ' ')
	{
		throw IndentationException();
	}
	
	Rtrim(str);

	if(depth == 0)
	{
		continueCheckingValue = !makeAssignment(str);
		if (!continueCheckingValue)
		{
			returnValue = std::make_shared<Void>();
			returnValue->setTemp(true);
		}
	}



	
	if(continueCheckingValue)
	{
		if(std::regex_search(str, s_TypeFunctionExpression))
		{
			return std::make_shared<String>(parseString(str.substr(5, str.size() - 6))->getTypeString());
		}
		if(std::regex_search(str, s_SizeFunctionExpression))
		{
			auto ptr = std::dynamic_pointer_cast<Sequence>(parseString(str.substr(4, str.size() - 5)));
			if(ptr)
			{
				return std::make_shared<Integer>(ptr->getLength());
				
			}
			else
			{
				throw TypeExcpetion(parseString(str.substr(4, str.size() - 5))->getTypeString());
			}
		}
		
		if (str.substr(0, 4) == "del ")
		{
			std::string varName = str.substr(4);
			if (m_Variables.Variables.count(varName))
			{
				m_Variables.Variables.erase(varName);
				std::cout << "Deleted " << varName << '\n';
				returnValue = std::make_shared<Void>();
			}
			else
			{
				throw NameErrorException(varName);
			}
		}
		if(IsList(str))
		{
			returnValue = std::make_shared<List>();
			std::sregex_iterator end;
			std::sregex_iterator reIterator(str.begin(), str.end(), s_ListElementExpression);
			
			while(reIterator != end)
		    {
				std::dynamic_pointer_cast<List>(returnValue)->append(parseString(reIterator->str()));
		        ++reIterator;
		    }
		}
		if(IsNumber(str))
		{
			return std::make_shared<Integer>(Integer(std::stoi(str)));
		}

		if (IsBoolean(str))
		{
			return std::make_shared<Boolean>(str == "True");
		}

		if (IsString(str))
		{
			return std::make_shared<String>(std::string(str.begin() + 1, str.end() - 1));
		}

		if(isLegalVarName(str)) 
		{
			returnValue = getVariableValue(str);
			if (returnValue == nullptr)
			{
				throw NameErrorException(str);
			}
		}
	}
	return returnValue;
}


static std::shared_ptr<Object> CopyVar(std::shared_ptr<Object> object)
{
	if (object)
	{
		switch(object->getType())
		{
		case Types::Integer: return std::make_shared<Integer>(std::dynamic_pointer_cast<Integer>(object)->getValue());
		case Types::String: return std::make_shared<String>(std::dynamic_pointer_cast<String>(object)->getValue());
		case Types::Void: return std::make_shared<Void>();
		case Types::Boolean: return std::make_shared<Boolean>(std::dynamic_pointer_cast<Boolean>(object)->getValue());
		}
	}
	return nullptr;
	
}

Parser::~Parser()
{
	for(auto&[varName, varValue] : m_Variables.Variables)
	{
		std::cout << "Deleted " << varName << "!\n";
	}
}

bool Parser::isLegalVarName(const std::string& str)
{
	return !(str[0] >= '0' && str[0] <= '9') && std::all_of(str.begin(), str.end(), [](char character)
		{
			return (character >= '0' && character <= '9') || (character >= 'a' && character <= 'z') || (character >= 'A' && character <= 'Z') && character == '_';
		});
}

bool Parser::makeAssignment(const std::string& str)
{
	bool goodAssignment = false;
	auto equalsLocation = str.find('=');
	
	if (CharacterAmount(str, '=') == 1)
	{
		std::string variableName = Trim(str.substr(0, equalsLocation));

		if(isLegalVarName(variableName))
		{
			std::shared_ptr<Object> value = parseString(Trim(str.substr(equalsLocation + 1)), 1);

			if(value != nullptr)
			{
				auto variableIterator = m_Variables.Variables.find(variableName);

				if(variableIterator != m_Variables.Variables.end())
				{
					variableIterator->second = value; // CopyVar(value);
				}
				else
				{
					m_Variables.Variables[variableName] = value; //CopyVar(value);
				}

				goodAssignment = true;
			}
			else
			{
				throw SyntaxException();
			}
			
		}
	}
	return goodAssignment;
}

std::shared_ptr<Object> Parser::getVariableValue(const std::string& str)
{
	auto variableIterator = m_Variables.Variables.find(str);
	
	if (variableIterator != m_Variables.Variables.end())
	{
		return m_Variables.Variables[str];
	}
	return nullptr;
}


