﻿#include "String.h"
#include <algorithm>

static unsigned int CharacterAmount(const std::string& string, char character)
{
	return std::ranges::count(string, character);
}

String::String(const std::string& string)
	: m_Value(string)
{
	
}

std::string String::toString() const
{
	if(CharacterAmount(m_Value, '\'') > 0)
	{
		return '"' + m_Value + '"';
	}
	else
	{
		return '\'' + m_Value + '\'';
	}
}

Types String::getType() const
{
	return Types::String;
}
