#ifndef NAME_ERROR_EXCEPTION_H
#define NAME_ERROR_EXCEPTION_H
#include "InterperterException.h"
#include <string>

class NameErrorException : public InterperterException
{
public:
	explicit NameErrorException(const std::string& variableName);

	virtual const char* what() const override;
private:
	std::string m_ErrorMessage;
};


#endif // NAME_ERROR_EXCEPTION_H