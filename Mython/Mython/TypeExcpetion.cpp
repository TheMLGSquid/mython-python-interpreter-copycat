#include "TypeExcpetion.h"

TypeExcpetion::TypeExcpetion(const std::string& typeName)
	: m_ErrorMessage("Type error: " + typeName + " has no member len()")
{
}

const char* TypeExcpetion::what() const
{
	return m_ErrorMessage.c_str();
}
