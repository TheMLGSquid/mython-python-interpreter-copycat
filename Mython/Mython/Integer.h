#pragma once
#include "Object.h"


class Integer :
	public Object
{
public:
	explicit Integer(int value);

	std::string toString() const override;

	virtual Types getType() const override;
	
	int getValue() const { return m_Value; }
	virtual std::string getTypeString() const override { return "Integer"; };



private:
	int m_Value;
};

