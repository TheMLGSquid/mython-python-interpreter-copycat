#ifndef TYPE_H
#define TYPE_H
#include <string>

#define NAME_OF(x) (#x)

enum class Types
{
	Integer,
	String,
	Void,
	Sequence,
	Object,
	Boolean,
	List
};

class Object
{
public:
	virtual ~Object() = default;
	Object();
	bool isTemp() const;
	void setTemp(bool isTemp);
	virtual bool isPrintable() const;
	virtual std::string toString() const = 0;
	virtual Types getType() const = 0;
	
	virtual std::string getTypeString() const = 0;

private:
	bool m_IsTemp = false;
};

#endif //TYPE_H
