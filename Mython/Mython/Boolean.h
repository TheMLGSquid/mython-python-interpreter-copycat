#pragma once
#include "Object.h"
class Boolean :
	public Object
{
public:
	explicit Boolean(bool value);
	virtual std::string toString() const override;
	Types getType() const override;

	bool getValue() const { return m_Value; }

	virtual std::string getTypeString() const override { return "Boolean"; };

private:
	bool m_Value;
};

