#ifndef SYNTAX_EXCEPTION_H
#define SYNTAX_EXCEPTION_H

#include "InterperterException.h"

class SyntaxException : public InterperterException
{
	const char* what() const throw() override;

};


#endif // SYNTAX_EXCEPTION_H