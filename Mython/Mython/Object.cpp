#include "Object.h"

Object::Object()
{
}

bool Object::isTemp() const
{
	return m_IsTemp;
}

void Object::setTemp(bool isTemp)
{
	m_IsTemp = isTemp;
}

bool Object::isPrintable() const
{
	return true;
}
