﻿#pragma once
#include "Sequence.h"


class String : public Sequence
{
public:
	explicit String(const std::string& string);
	virtual std::string toString() const override;
	Types getType() const override;
	
	const std::string& getValue() const { return m_Value; }
	virtual std::string getTypeString() const override { return "String"; };
	virtual unsigned int getLength() const override { return m_Value.size(); }

private:
	std::string m_Value;
};
