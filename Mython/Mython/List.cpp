#include "List.h"

void List::append(std::shared_ptr<Object> object)
{
	if(object)
	{
		m_Objects.push_back(object);
	}
}

std::string List::toString() const
{
	std::string fullString = "[";

	for(const auto& value : m_Objects)
	{
		if(value)
		{
			fullString += value->toString() + ", ";
		}
	}
	fullString.pop_back();
	fullString.pop_back();
	fullString += ']';
	return fullString;
}

Types List::getType() const
{
	return Types::List;
}
