#include "Void.h"


Types Void::getType() const
{
	return Types::Void;
}

bool Void::isPrintable() const
{
	return false;
}

std::string Void::toString() const
{
	return "";
}
