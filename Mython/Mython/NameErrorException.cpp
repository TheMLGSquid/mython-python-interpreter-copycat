#include "NameErrorException.h"

NameErrorException::NameErrorException(const std::string& variableName)
	: m_ErrorMessage("Name Error: name '" + variableName + "' is not defined")
{
}

const char* NameErrorException::what() const
{
	return m_ErrorMessage.c_str();
}
