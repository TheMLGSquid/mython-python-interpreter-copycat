#ifndef PARSER_H
#define PARSER_H

#include "Object.h"
#include <string>
#include <unordered_map>
#include "StackFunny.h"




class Parser
{
public:
	std::shared_ptr<Object> parseString(std::string str, int depth = 0);
	~Parser();
private:

	static bool isLegalVarName(const std::string& str);
	bool makeAssignment(const std::string& str);
	std::shared_ptr<Object> getVariableValue(const std::string &str);
private:	
	StackFunny m_Variables;
};

#endif //PARSER_H
