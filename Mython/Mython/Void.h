#pragma once
#include "Object.h"
class Void :
	public Object
{
public:
	Types getType() const override;
	Void(void) = default;
	bool isPrintable() const override;
	std::string toString() const override;
	void getValue() const {  } //lmao
	virtual std::string getTypeString() const override { return "Void"; };

};

