#include "Object.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>
#include <string>
#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Tal Shachar"
#include "SyntaxException.h"


int main(int argc,char **argv)
{
	Parser parser;
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string inputString;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, inputString);
	
	while ("quit()" != inputString)
	{
		// parsing command
		try
		{
			std::shared_ptr<Object> object = parser.parseString(inputString); 
			if (object != nullptr)
			{
				if(object->isPrintable())
				{
					std::cout << object->toString() << '\n';
				}
			}
			else
			{
				throw SyntaxException();
			}
		}
		catch (const InterperterException& e)
		{
			std::cout << e.what() << std::endl;
		}

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, inputString);
	}

	return 0;
}


