#include "Integer.h"

Integer::Integer(int value)
	: m_Value(value)
{
}

std::string Integer::toString() const
{
	return std::to_string(m_Value);
}

Types Integer::getType() const
{
	return Types::Integer;
}
