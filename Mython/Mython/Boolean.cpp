#include "Boolean.h"

Boolean::Boolean(bool value)
	: m_Value(value)
{
	
}

std::string Boolean::toString() const
{
	return m_Value ? "True" : "False";
}

Types Boolean::getType() const
{
	return Types::Boolean;
}
